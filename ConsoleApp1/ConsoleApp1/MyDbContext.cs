﻿using ConsoleApp1.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace EFCore
{
    public class MyDbContext : DbContext
    {
        public MyDbContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<PersonEntity>(Build);
        }

        private void Build(EntityTypeBuilder<PersonEntity> entity)
        {
            entity.ToTable("Person");
            entity.HasKey(pk => pk.PersonId);
        }

        public DbSet<PersonEntity> People { get; set; }
    }
}
